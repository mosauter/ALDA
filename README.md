ALDA
====

This is a Repository for the Algorithms and Datasturctures. It contains three Tasks.

#### Task 1 ####

An implementation of a Dictionary. With a GUI and various implementations of the Dictionary.
As a:
* *HashMap of the Java library*
* *TreeMap of the Java library*
* *own implementation of a Sorted Array*
* *own implementation of a Hash Map*
* *own implementation of a Tree Map*

#### Task 2 ####

An implementation of a Graph. Two implementations as a:
* *Undirected Graph*
* *Directed Graph*

Additionally it implements a Deepth- and Breadth-Search. To test them I used the SYSimulation library. It attachs on the known Board-Game 'Scotland Yard'. To simplify the test only the Taxi-Routes are added.

Also implements the Dijkstra-Algorithm to search the shortest Path between to Vertexes of the Graph. 
To test this Algorithm I used the Bus- and the Subway-Routes as well. If there were more then one Connection between two vertexes, I used the cheaper one.
