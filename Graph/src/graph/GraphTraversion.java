// GraphTraversion.java

package graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

/**
 * GraphTraversion implements Searches for Graphs.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @param <V> Vertex-Type
 * @since 2014-11-19
 */
public final class GraphTraversion<V> {

    /**
     * Private Constructor.
     */
    private GraphTraversion() { }

    /**
     * Method for the Breadth-Search.
     * @param <V> specified Vertex-Type
     * @param g Graph
     * @param s vertex where to start
     * @return List<V> which contains all visited vertexes
     */
    public static <V> List<V> breadthFirstSearch(final Graph<V> g, V s) {
        List<V> besucht = new LinkedList<>();
        Queue<V> queue = new LinkedList<>();
        queue.add(s);
        while (!queue.isEmpty()) {
            s = queue.remove();

            if (besucht.contains(s)) {
                continue;
            }
            besucht.add(s);

            for (V vertex : g.getAdjacentVertexList(s)) {
                if (!besucht.contains(vertex)) {
                    queue.add(vertex);
                }
            }
        }
        return besucht;
    }

    /**
     * Method for topological Sort.
     * @param <V> generic Type
     * @param g DirectedGraph
     * @return List<V>
     */
    public static <V> List<V> topologicalSort(final DirectedGraph<V> g) {
        List<V> ts = new LinkedList<>();
//        int[] inDegree = new int[g.getNumberOfVertexes()];
        HashMap<V, Integer> inDegree = new HashMap<>();
        Queue<V> q = new LinkedList<>();

        for (V vertex : g.getVertexList()) {
            inDegree.put(vertex, g.getPredecessorVertexList(vertex).size());
            if (inDegree.get(vertex) == 0) {
                q.add(vertex);
            }
        }

        List<V> verList = g.getVertexList();
        while (!q.isEmpty()) {
            V vertex = q.remove();
            ts.add(vertex);

            for (V after : g.getSuccessorVertexList(vertex)) {
                inDegree.put(after, inDegree.get(after) - 1);
                if (inDegree.get(after) == 0) {
                    q.add(after);
                }
            }
        }

        if (ts.size() != g.getNumberOfVertexes()) {
            throw new IllegalArgumentException("Zyklischer Graph!");
        }
        return ts;
    }

    /**
     * Start method for recursive Depth-Search.
     * @param <V> specified Vertex-type
     * @param g Graph
     * @param s Vertex where to start
     * @return List<V> which contains all visited vertexes.
     */
    public static <V> List<V> depthFirstSearch(final Graph<V> g, final V s) {
        List<V> besucht = new LinkedList<>();
        return depthFirstSearchR(g, s, besucht);
    }

    /**
     * Private recursive Method for the Depth-Search.
     * @param <V> specified Vertex-Type
     * @param g the graph
     * @param s the vertex where to start
     * @param besucht List which vertexes allready visited
     * @return List<V> which contains all visited vertexes
     */
    private static <V> List<V> depthFirstSearchR(final Graph<V> g, final V s,
            List<V> besucht) {
        if (besucht.size() == g.getNumberOfVertexes()) {
            return besucht;
        }
        besucht.add(s);
        for (V vertex : g.getAdjacentVertexList(s)) {
            if (!besucht.contains(vertex)) {
                besucht = depthFirstSearchR(g, vertex, besucht);
            }
        }
        return besucht;
    }
}
