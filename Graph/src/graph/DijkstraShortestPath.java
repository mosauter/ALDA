// DijkstraShortestPath.java

package graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;


/**
 * DijkstraShortestPath.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @param <V> generic type
 * @since 2014-12-02
 */
public class DijkstraShortestPath<V> {

    /**
     * To save the graph.
     */
    private final Graph<V> graph;
    /**
     * Boolean if the search was a success.
     */
    private boolean success = false;
    /**
     * Distance of the shortest path.
     */
    private double distance;
    /**
     * List which describes the shortest path.
     */
    private List<V> shortestPath;

    /**
     * Public-Constructor.
     * @param g Graph<V>
     */
    public DijkstraShortestPath(final Graph<V> g) {
        graph = g;
    }

    /**
     * Method to search the shortest Path between two vertexes.
     * @param start start vertex
     * @param end destination vertex
     * @return true if there is a path between s and g
     */
    public final boolean searchShortestPath(final V start, final V end) {
        List<V> candidates = graph.getAdjacentVertexList(start);
        HashMap<V, Double> d = new HashMap<>();
        List<V> vertList = graph.getVertexList();
        HashMap<V, V> p = new HashMap<V, V>();
        for (V vertex : vertList) {
            p.put(vertex, null);
            d.put(vertex, Double.MAX_VALUE);
        }
        d.put(start, 0.);
        candidates.add(start);
        while (!candidates.isEmpty()) {
            double doub = Double.MAX_VALUE;
            int index = 0;
            V candidate = null;
            for (V cand : candidates) {
                if (cand.equals(end)) {
                    if (d.get(cand) < doub) {
                        doub = d.get(cand);
                        candidate = cand;
                    }
                    continue;
                }
                if ((d.get(cand) + graph.getWeight(cand, end)) < doub) {
                    doub = d.get(cand) + graph.getWeight(cand, end);
                    candidate = cand;
                }
            }
            if (candidate.equals(end)) {
                LinkedList<V> l = new LinkedList<>();
                l.push(end);
                V help = p.get(candidate);
                while (help != start) {
                    l.push(help);
                    help = p.get(help);
                }
                l.push(help);
                this.shortestPath = l;
                distance = doub;
                success = true;
                return true;
            }
            candidates.remove(candidate);
            for (V ver : graph.getAdjacentVertexList(candidate)) {
                if (d.get(ver) == Double.MAX_VALUE) {
                    candidates.add(ver);
                }
                if ((d.get(candidate) + graph.getWeight(candidate, ver)) < d.get(ver)) {
                    p.put(ver, candidate);
                    double leng = d.get(candidate) + graph.getWeight(candidate, ver);
                    d.put(ver, leng);
                }
            }
        }
        success = false;
        return false;
    }

    /**
     * Getter for the shortest Path list.
     * @return List<V>
     */
    public final List<V> getShortestPath() {
        if (!success) {
            throw new IllegalStateException("Suche war nicht erfolgreich!");
        }
        return shortestPath;
    }

    /**
     * Getter for the Distance.
     * @return double
     */
    public final double getDistance() {
        if (!success) {
            throw new IllegalStateException("Suche war nicht erfolgreich!");
        }
        return distance;
    }
}
