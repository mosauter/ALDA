// GraphTest.java

package graph;

import java.awt.Color;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.Iterator;
import java.util.List;
import sim.SYSimulation;

/**
 * GraphTest is a Test class for the graph-implementations.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2014-11-19
 */
public final class GraphTest {

    /**
     * Constant for 3.
     */
    private static final int THREE = 3;
    /**
     * Constant for 200.
     */
    private static final int TWOHUNDRED = 200;
    /**
     * Constant for the Dijkstra-Algorithm where to start.
     */
    private static final int VERTEXSTART = 1;
    /**
     * Constant for the Dijkstra-Algorithm where to end.
     */
    private static final int VERTEXEND = 42;
    /**
     * Constant for TAXI - Costs.
     */
    private static final int TAXI = 3;
    /**
     * Constant for BUS - Costs.
     */
    private static final int BUS = 2;
    /**
     * Constant for U-BAHN - Costs.
     */
    private static final int UBAHN = 5;

    /**
     * Private Constructor.
     */
    private GraphTest() { }

    /**
     * Constant for 10.
     */
    private static final int VERTEX_NUM = 10;

    /**
     * Private static inner class for the Graph-Test.
     */
    private static class Vertex {
        /**
         * Saves an Integer.
         */
        private final Integer value;

        /**
         * Public constructor.
         * @param value specified integer
         */
        public Vertex(final int value) {
            this.value = value;
        }

        /**
         * Getter for value.
         * @return an Integer
         */
        public int getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value.toString();
        }
    }

    /**
    * Private static inner class for the Graph-Test with Strings.
    */
    private static class VertexString {
        /**
         * Saves an String.
         */
        private final String value;

        /**
         * Public constructor.
         * @param value specified string
         */
        public VertexString(final String value) {
            this.value = value;
        }

        /**
         * Getter for value.
         * @return an String
         */
        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return value;
        }
    }

    /**
     * To test topological Sort.
     */
    private static void topSearch() {
        DirectedGraph<VertexString> graphString =
                new AdjacencyListDirectedGraph<>();
        System.out.println("\nTopological Sort");
        VertexString[] vList = {new VertexString("Struempfe"),
                                new VertexString("Schuhe"),
                                new VertexString("Hose"),
                                new VertexString("Unterhose"),
                                new VertexString("Unterhemd"),
                                new VertexString("Hemd"),
                                new VertexString("Guertel"),
                                new VertexString("Pullover"),
                                new VertexString("Mantel"),
                                new VertexString("Schal"),
                                new VertexString("Handschuhe"),
                                new VertexString("Muetze") };

        for (VertexString vertex : vList) {
            graphString.addVertex(vertex);
        }

        graphString.addEdge(vList[3], vList[2]);
        graphString.addEdge(vList[3], vList[4]);
        graphString.addEdge(vList[2], vList[6]);
        graphString.addEdge(vList[6], vList[0]);
        graphString.addEdge(vList[0], vList[1]);
        graphString.addEdge(vList[1], vList[8]);
        graphString.addEdge(vList[8], vList[9]);
        graphString.addEdge(vList[9], vList[10]);
        graphString.addEdge(vList[10], vList[11]);

        System.out.println(GraphTraversion.topologicalSort(graphString));
    }

    /**
     * Test of the Graph implementations.
     */
    private static void testOther() {
        Graph<Vertex> graph1 = new AdjacencyListUndirectedGraph<>();
        Graph<Vertex> graph2 = new AdjacencyListDirectedGraph<>();
        Vertex[] vertexes = new Vertex[VERTEX_NUM];

        for (int i = 0; i < VERTEX_NUM; i++) {
            vertexes[i] = new Vertex(i);
        }
        for (Vertex vertex : vertexes) {
            graph1.addVertex(vertex);
            graph2.addVertex(vertex);
        }
        List<Vertex> list1 = graph1.getVertexList();
        List<Vertex> list2 = graph2.getVertexList();
        System.out.println(list1);
        System.out.println(list2);

        System.out.println("Edges created");
        for (int i = 0; i < vertexes.length; i++) {
            int k = (i + 1) % vertexes.length;
            graph1.addEdge(vertexes[i], vertexes[k], i);
            graph2.addEdge(vertexes[i], vertexes[k], i);
        }

        List<Vertex> searchList1 =
                GraphTraversion.depthFirstSearch(graph1, vertexes[0]);
        List<Vertex> searchList2 =
                GraphTraversion.breadthFirstSearch(graph1, vertexes[0]);
        List<Vertex> searchList3 =
                GraphTraversion.depthFirstSearch(graph2, vertexes[0]);
        List<Vertex> searchList4 =
                GraphTraversion.breadthFirstSearch(graph2, vertexes[0]);

        System.out.println("Search graph1 print");
        System.out.println(searchList1);
        System.out.println(searchList2);
        System.out.println("Search graph2 print");
        System.out.println(searchList3);
        System.out.println(searchList4);
    }

    /**
     * Method to test the Scotland.
     * @throws IOException if one of the used Methods throws IO
     */
    private static void testScotland() throws IOException {
        Graph<Integer> scotland = new AdjacencyListUndirectedGraph<>();
        createScotlandTaxi(scotland);

        printSim(scotland, GraphTraversion.breadthFirstSearch(scotland, 1),
                "Breadth Search");
        printSim(scotland, GraphTraversion.depthFirstSearch(scotland, 1),
                "Deepth Search");
        scotland = new AdjacencyListUndirectedGraph<>();
        createScotlandAll(scotland);
        DijkstraShortestPath<Integer> di = new DijkstraShortestPath<>(scotland);
        di.searchShortestPath(VERTEXSTART, VERTEXEND);
        printSim(scotland, di.getShortestPath(), "Dijkstra Algorithm");
        System.out.printf("Distance: %f%n", di.getDistance());
        System.exit(0);
    }

    /**
     * Create a Graph with the Scotland Game.
     * @param scotland where to save the Graph
     * @throws IOException if the file not exists
     */
    private static void createScotlandTaxi(final Graph<Integer> scotland)
            throws IOException {
        for (int i = 1; i < TWOHUNDRED; i++) {
            scotland.addVertex(i);
        }
        FileReader file = new FileReader("/home/moritz/Dokumente/"
                + "NetBeansProjects/ALDA/ALDA/"
                + "Graph/src/graph/ScotlandYard.txt");
        try (LineNumberReader in = new LineNumberReader(file)) {
            String line;
            while ((line = in.readLine()) != null) {
                String[] sf = line.split(" ");
                if (sf.length == THREE) {
                    if (sf[2].equals("Taxi")) {
                        int verOne = Integer.parseInt(sf[0]);
                        int verTwo = Integer.parseInt(sf[1]);
                        scotland.addEdge(verOne, verTwo);
                    }
                }
            }
        }
    }

    /**
     * Method to create the Scotland Graph with Bus, Taxi and U-Bahn.
     * @param scotland Graph
     * @throws IOException if file not exists
     */
    private static void createScotlandAll(final Graph<Integer> scotland)
            throws IOException {
        for (int i = 0; i < TWOHUNDRED; i++) {
            scotland.addVertex(i);
        }
        FileReader file = new FileReader("/home/moritz/Dokumente/"
                + "NetBeansProjects/ALDA/ALDA/"
                + "Graph/src/graph/ScotlandYard.txt");
        LineNumberReader in = new LineNumberReader(file);
        String line;
        while ((line = in.readLine()) != null) {
            String[] sf = line.split(" ");
            if (sf.length == THREE) {
                int verOne = Integer.parseInt(sf[0]);
                int verTwo = Integer.parseInt(sf[1]);
                boolean exists = scotland.containsEdge(verOne, verTwo);
                double weight = 0;
                if (exists) {
                    weight = scotland.getWeight(verOne, verTwo);
                }
                switch (sf[2]) {
                    case "Taxi":
                        if (exists) {
                            if (weight < TAXI) {
                                break;
                            }
                        }
                        scotland.addEdge(verOne, verTwo, TAXI);
                        break;
                    case "Bus":
                        scotland.addEdge(verOne, verTwo, BUS);
                        break;
                    case "UBahn":
                        if (exists) {
                            break;
                        }
                        scotland.addEdge(verOne, verTwo, UBAHN);
                        break;
                    default:
                        break;
                }
            }
        }
    }

    /**
     * Method to print the Scotland Graph.
     * @param scotland Graph<Integer>
     * @param list List
     * @param name name of the sequence
     * @throws IOException if the creation of the SYSimulation fails
     */
    public static void printSim(final Graph<Integer> scotland,
            final List<Integer> list, final String name)
            throws IOException {
        SYSimulation sim = new SYSimulation();
        sim.startSequence(name);
        for (Integer i : list) {
            sim.visitStation(i);
        }

        Iterator<Integer> iter1 = list.iterator();
        Iterator<Integer> iter2 = list.iterator();
        iter2.next();
        while (iter1.hasNext()) {
            int i = iter1.next();
            if (!iter2.hasNext()) {
                break;
            }
            int j = iter2.next();
            switch ((int) scotland.getWeight(i, j)) {
                case 1:
                    sim.drive(i, j, Color.MAGENTA);
                    break;
                case BUS:
                    sim.drive(i, j, Color.GREEN);
                    break;
                case TAXI:
                    sim.drive(i, j, Color.YELLOW);
                    break;
                case UBAHN:
                    sim.drive(i, j, Color.RED);
                    break;
                default:
                    break;
            }
        }
        sim.stopSequence();
    }

    /**
     * Main - Method.
     * @param args not used
     * @throws java.io.IOException if components throws Exceptions
     */
    public static void main(final String[] args) throws IOException {
        testOther();
        topSearch();
        testScotland();
    }
}
