// AdjacencyListDirectedGraph.java

package graph;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * AdjacencyListDirectedGraph implements a directed Graph.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @param <V> specified Vertex-Object
 * @since 2014-11-18
 */
public class AdjacencyListDirectedGraph<V> implements DirectedGraph<V> {

    /**
     * HashMap with the Edges which are leading to the vertex.
     */
    private HashMap<V, HashMap<V, Double>> afterList = new HashMap<>();
    /**
     * HashMap with the Edges which are coming form the vertex.
     */
    private HashMap<V, HashMap<V, Double>> befList = new HashMap<>();
    /**
     * Saves how much Edges are stored in the Graph.
     */
    private int numOfEdges = 0;
    /**
     * Saves how much Vertexes are stored in the Graph.
     */
    private int numOfVertex = 0;

    @Override
    public final int getInDegree(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex not existing.");
        }
        return afterList.get(v).size();
    }

    @Override
    public final int getOutDegree(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex not existing.");
        }
        return befList.get(v).size();
    }

    @Override
    public final List<V> getPredecessorVertexList(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex not existing.");
        }
        List<V> vertexList = new LinkedList<>();
        for (V vertex : afterList.get(v).keySet()) {
            vertexList.add(vertex);
        }
        return vertexList;
    }

    @Override
    public final List<V> getSuccessorVertexList(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex not existing.");
        }
        List<V> vertexList = new LinkedList<>();
        for (V vertex : befList.get(v).keySet()) {
            vertexList.add(vertex);
        }
        return vertexList;
    }

    @Override
    public final List<Edge<V>> getOutgoingEdgeList(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex not existing.");
        }
        List<Edge<V>> edgList = new LinkedList<>();
        for (V vertex : befList.get(v).keySet()) {
            edgList.add(new Edge<>(v, vertex, befList.get(v).get(vertex)));
        }
        return edgList;
    }

    @Override
    public final List<Edge<V>> getIncomingEdgeList(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex not existing.");
        }
        List<Edge<V>> edgList = new LinkedList<>();
        for (V vertex : afterList.get(v).keySet()) {
            edgList.add(new Edge<>(v, vertex, afterList.get(v).get(vertex)));
        }
        return edgList;
    }

    @Override
    public final boolean addVertex(final V v) {
        if (afterList.containsKey(v)) {
            return false;
        }
        afterList.put(v, new HashMap<>());
        befList.put(v, new HashMap<>());
        numOfVertex++;
        return true;
    }

    @Override
    public final boolean addEdge(final V v, final V w) {
        return addEdge(v, w, 1);
    }

    @Override
    public final boolean addEdge(final V v, final V w, final double weight) {
        if (containsEdge(v, w)) {
            afterList.get(w).put(v, weight);
            befList.get(v).put(w, weight);
            return false;
        }
        afterList.get(w).put(v, weight);
        befList.get(v).put(w, weight);
        numOfEdges++;
        return true;
    }

    @Override
    public final boolean containsVertex(final V v) {
        return afterList.containsKey(v);
    }

    @Override
    public final boolean containsEdge(final V v, final V w) {
        if (!containsVertex(v) || !containsVertex(w) || v == w) {
            throw new IllegalArgumentException("Minimum one of the"
                    + " vertexes is not existing.");
        }
        return afterList.get(w).containsKey(v);
    }

    @Override
    public final double getWeight(final V v, final V w) {
        if (!containsEdge(v, w)) {
            return 0;
        }
        return afterList.get(v).get(w);
    }

    @Override
    public final int getNumberOfVertexes() {
        return numOfVertex;
    }

    @Override
    public final int getNumberOfEdges() {
        return numOfEdges;
    }

    @Override
    public final List<V> getVertexList() {
        List<V> verList = new LinkedList<>();
        for (V vertex : afterList.keySet()) {
            verList.add(vertex);
        }
        return verList;
    }

    @Override
    public final List<Edge<V>> getEdgeList() {
        List<Edge<V>> edgList = new LinkedList<>();
        for (V key : befList.keySet()) {
            for (V vertex : befList.get(key).keySet()) {
                edgList.add(new Edge<>(key, vertex,
                        befList.get(key).get(vertex)));
            }
        }
        return edgList;
    }

    @Override
    public final List<V> getAdjacentVertexList(final V v) {
        return getSuccessorVertexList(v);
    }

    @Override
    public final List<Edge<V>> getIncidentEdgeList(final V v) {
        return getOutgoingEdgeList(v);
    }
}