// AdjacencyListUndirectedGraph.java

package unionfind;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * AdjacencyListUndirectedGraph is an implementation for the UndirectedGraph.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @param <V> specified Vertex-Object
 * @since 2014-11-16
 */
public class AdjacencyListUndirectedGraph<V> implements UndirectedGraph<V> {

    /**
     * HashMap where the graph is saved.
     */
    private final HashMap<V, HashMap<V, Double>> adjacencyList = new HashMap<>();
    /**
     * How much Edges there are in the graph.
     */
    private int numOfEdges = 0;
    /**
     * How much Vertexes there are in the graph.
     */
    private int numOfVertex = 0;

    @Override
    public final int getDegree(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex is not existing.");
        }
        return adjacencyList.get(v).size();
    }

    @Override
    public final boolean addVertex(final V v) {
        if (adjacencyList.containsKey(v)) {
            return false;
        }
        adjacencyList.put(v, new HashMap<>());
        numOfVertex++;
        return true;
    }

    @Override
    public final boolean addEdge(final V v, final V w) {
        return addEdge(v, w, 1);
    }

    @Override
    public final boolean addEdge(final V v, final V w, final double weight) {
        if (v.equals(w)) {
            throw new IllegalArgumentException("Vertexes are the same.");
        }

        if (containsEdge(v, w)) {
            adjacencyList.get(v).put(w, weight);
            adjacencyList.get(w).put(v, weight);
            return false;
        }

        adjacencyList.get(v).put(w, weight);
        adjacencyList.get(w).put(v, weight);
        numOfEdges++;
        return true;
    }

    @Override
    public final boolean containsVertex(final V v) {
        return adjacencyList.containsKey(v);
    }

    @Override
    public final boolean containsEdge(final V v, final V w) {
        if (!containsVertex(v) || !containsVertex(w)) {
            throw new IllegalArgumentException("Minimum one of the "
                    + "Vertexes is not existing.");
        }
        if (v.equals(w)) {
            throw new IllegalArgumentException("The vertexes are the same.");
        }
        return adjacencyList.get(v).containsKey(w);
    }

    @Override
    public final double getWeight(final V v, final V w) {
        if (!containsEdge(v, w)) {
            return 0;
        }
        return adjacencyList.get(v).get(w);
    }

    @Override
    public final int getNumberOfVertexes() {
        return numOfVertex;
    }

    @Override
    public final int getNumberOfEdges() {
        return numOfEdges;
    }

    @Override
    public final List<V> getVertexList() {
        List<V> listV = new LinkedList<>();
        for (V vertex : adjacencyList.keySet()) {
            listV.add(vertex);
        }
        return listV;
    }

    @Override
    public final List<Edge<V>> getEdgeList() {
        List<Edge<V>> edgeList = new LinkedList<>();
        for (V vertex1 : adjacencyList.keySet()) {
            HashMap<V, Double> mapV = adjacencyList.get(vertex1);
            for (V vertex2 : mapV.keySet()) {
                Edge<V> edge = new Edge<>(vertex1, vertex2, mapV.get(vertex2));
                edgeList.add(edge);
            }
        }
        return edgeList;
    }

    @Override
    public final List<V> getAdjacentVertexList(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex is not existing!");
        }
        List<V> list = new LinkedList<>();
        for (V vertex : adjacencyList.get(v).keySet()) {
            list.add(vertex);
        }
        return list;
    }

    @Override
    public final List<Edge<V>> getIncidentEdgeList(final V v) {
        if (!containsVertex(v)) {
            throw new IllegalArgumentException("Vertex is not existing!");
        }
        List<Edge<V>> list = new LinkedList<>();
        List<V> listVertex = getAdjacentVertexList(v);
        for (V vertex : listVertex) {
            list.add(new Edge<>(v, vertex, adjacencyList.get(v).get(vertex)));
        }
        return list;
    }
}