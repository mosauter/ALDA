// TelNet.java

package unionfind;

import java.awt.Color;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;

/**
 * TelNet
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2015-01-18
 */
public class TelNet {

    public int LBG;
    
    private final UndirectedGraph<Integer> g;

    private TelKnoten[] telKnoten;
    private int telKnotenSize;

    private boolean calculated;

    private List<TelVerbindung> minSpanTree;
    private final int DEF_WAIT_TIME = 30000;

    /**
     * public.
     * @param LBG Leistungsbegrenzer
     */
    public TelNet(int LBG) {
        this.LBG = LBG;
        this.g = new AdjacencyListUndirectedGraph<>();
        this.telKnoten = new TelKnoten[256];
        this.telKnotenSize = 1;
    }

    /**
     * calculates the manhatten distance.
     * @param t1 first node
     * @param t2 second node
     * @return the costs
     */
    private int cost(final TelKnoten t1, final TelKnoten t2) {
        int cost = Math.abs(t1.x - t2.x) + Math.abs(t1.y - t2.y);
        if (cost > LBG) {
            return -1;
        }
        return cost;
    }

    /**
     * to ensure the capacity of the array.
     */
    private void ensureCapacity() {
        if (this.telKnotenSize >= this.telKnoten.length - 1) {
            TelKnoten[] telKnotenNew = Arrays.copyOf(telKnoten,
                    this.telKnotenSize * 2);
            this.telKnoten = telKnotenNew;
        }
    }

    /**
     * add a new node in the telnet.
     * @param x
     * @param y 
     */
    public void addTelKnoten(int x, int y) {
        ensureCapacity();
        TelKnoten ntk = new TelKnoten(x, y);
        int weight;
        this.telKnoten[telKnotenSize] = ntk;
        int number = telKnotenSize++;
        this.g.addVertex(number);
        for (Integer t1 : this.g.getVertexList()) {
            if (!t1.equals(number)) {
                weight = cost(ntk, this.telKnoten[t1]);
                if (weight != -1) {
                    this.g.addEdge(t1, number, weight);
                }
            }
        }
    }

    /**
     * calculates the minspantree.
     */
    public void computeOptTelNet() {
        UnionFind forest = new UnionFind(this.g.getNumberOfVertexes());
        PriorityQueue<Edge<Integer>> edges =
                new PriorityQueue(this.g.getNumberOfEdges(),
                        new EdgeComparator());
        for (Edge<Integer> e : this.g.getEdgeList()) {
            edges.add(e);
        }

        minSpanTree = new ArrayList<>();

        while (forest.size() != 1 && !edges.isEmpty()) {
            Edge<Integer> t = edges.poll();
            int rep1 = forest.find(t.source);
            int rep2 = forest.find(t.target);
            if (rep1 != rep2) {
                forest.union(rep1, rep2);
                minSpanTree.add(new TelVerbindung(this.telKnoten[t.source],
                        this.telKnoten[t.target]));
            }
        }

        if (forest.size() == 1) {
            this.calculated = true;
        }
    }

    /**
     * Getter for the minSpanTree.
     * @return the tree
     */
    public List<TelVerbindung> getOptTelNet() {
        if (this.calculated) {
            return this.minSpanTree;
        }
        throw new IllegalStateException("min span tree not calculated yet");
    }

    /**
     * Calculates the entire costs for the telnet
     * @return the costs
     */
    public int getOptTelNetCost() {
        int cost = 0;
        if (this.calculated) {
            for (TelVerbindung t1 : this.minSpanTree) {
                cost += cost(t1.u, t1.v);
            }
            return cost;
        }
        throw new IllegalStateException("min span tree not calculated yet");
    }

    /**
     * generate a random telnet.
     * @param n how many telnets
     * @param xMax max value
     * @param yMax min value
     */
    public void generateRandomTelNet(int n, int xMax, int yMax) {
        Random rnd = new Random();
        for (int i = 0; i < n; i++) {
            this.addTelKnoten(rnd.nextInt(xMax - 1) + 1,
                    rnd.nextInt(yMax - 1) + 1);
        }
    }

    public void drawOptTelNet(int xMax, int yMax) {
        if (!this.calculated) {
            throw new IllegalStateException("min span tree not calculated yet");
        }

        StdDraw.setCanvasSize(1000, 650);
        StdDraw.setXscale(0, xMax);
        StdDraw.setYscale(0, yMax);

        StdDraw.setPenColor(Color.BLUE);
        for (int i = 1; i < this.telKnotenSize; i++) {
            StdDraw.filledCircle(this.telKnoten[i].x, this.telKnoten[i].y, 2);
        }

        StdDraw.setPenColor(Color.RED);
        for (TelVerbindung t : this.minSpanTree) {
            StdDraw.line(t.u.x, t.u.y, t.v.x, t.u.y);
            StdDraw.line(t.v.x, t.u.y, t.v.x, t.v.y);
        }
        StdDraw.show(DEF_WAIT_TIME);
    }

    @Override
    public final String toString() {
        return "TelKnoten: [ " + this.g.getVertexList().toString() + " ]\n" +
                "Moegliche Kanten: [ " + this.g.getEdgeList().toString() + " ]";
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        UnionFind u = new UnionFind(20);
        u.union(5, 7);
        u.union(7, 1);
        u.union(3, 2);
        u.union(20, 3);
        u.union(19, 20);
        System.out.println(u.find(1)); // 5
        System.out.println(u.find(19)); // 3
        u.union(19, 1);
        System.out.println(u.find(1)); // 3

        TelNet tel = new TelNet(20);
        tel.addTelKnoten(1, 1);
        tel.addTelKnoten(3, 1);
        tel.addTelKnoten(4, 2);
        tel.addTelKnoten(3, 4);
        tel.addTelKnoten(7, 5);
        tel.addTelKnoten(2, 6);
        tel.addTelKnoten(4, 7);
        tel.computeOptTelNet();

        List<TelVerbindung> t = tel.getOptTelNet();
        System.out.println(t);

        int costs = tel.getOptTelNetCost();
        System.out.println("Costs: " + costs);

        TelNet tln = new TelNet(100);
        tln.generateRandomTelNet(5000, 1000, 2000);
        tln.computeOptTelNet();
        tln.drawOptTelNet(1000, 1000);
    }
}
