// UnionFind.java

package unionfind;

/**
 * UnionFind
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2015-01-18
 */
public final class UnionFind {

    /**
     * Array.
     */
    private final int[] p;

    /**
     * Public constructor.
     * @param n size of the struct
     */
    public UnionFind(final int n) {
        this.p = new int[n + 1];
    }

    /**
     * searches the parent union.
     * @param e the specified value which you have to find
     * @return the parent union
     */
    public final int find(int e) {
        if (e <= 0 || e >= p.length) {
            throw new IllegalArgumentException("the element not exists");
        }
        while (p[e] > 0) {
            e = p[e];
        }
        return e;
    }

    /**
     * Vereinigt Mengen von e1 und e2.
     * @param e1 union 1
     * @param e2 union 2
     */
    public final void union(final int e1, final int e2) {
        int rep1 = find(e1);
        int rep2 = find(e2);

        if (rep1 == rep2) {
            return;
        }

        if (-this.p[rep1] < -this.p[rep2]) {
            this.p[rep1] = rep2;
        } else {
            if (-this.p[rep1] == -this.p[rep2]) {
                this.p[rep1]--;
            }
            this.p[rep2] = rep1;
        }
    }

    /**
     * counts all the parent unions.
     * @return the size
     */
    public int size() {
        int s = 0;
        for (int i = 1; i < p.length; i++) {
            if (p[i] <= 0) {
                s++;
            }
        }
        return s;
    }
}
