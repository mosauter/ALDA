// TelVerbindung.java

package unionfind;

/**
 * TelVerbindung
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2015-01-18
 */
public class TelVerbindung {

    public final TelKnoten u;
    public final TelKnoten v;

    public TelVerbindung(TelKnoten u, TelKnoten v) {
        this.u = u;
        this.v = v;
    }

    @Override
    public final String toString() {
        return "TelVerbindung: { " + this.u.toString()
                + " - " + this.v.toString() + " }";
    }
}
