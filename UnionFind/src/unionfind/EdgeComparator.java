// EdgeComparator.java

package unionfind;

import java.util.Comparator;

/**
 * EdgeComparator
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2015-01-18
 */
public class EdgeComparator implements Comparator<Edge> {

    @Override
    public int compare(Edge o1, Edge o2) {
        return (int) (o1.weight - o2.weight);
    }
}
