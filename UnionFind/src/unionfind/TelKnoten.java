// TelKnoten.java

package unionfind;

/**
 * TelKnoten
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2015-01-18
 */
public class TelKnoten {
    public final int x;
    public final int y;

    public TelKnoten(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("TelKnoten (%d, %d)", x, y);
    }
}
