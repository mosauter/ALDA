// O. Bittel
// 19.9.2011

package dictionary;

/**
 * Utility-Interface for Dictionaries.
 * @param <K> key-type.
 * @param <V> value-type.
 */
public interface Dictionary<K,V> {
    /**
     * Associates the specified value with the specified key in this map.
     * If the map previously contained a mapping for the key,
     * the old value is replaced by the specivied value.
     * @param key specified key
     * @param value specified value
     * @return the previous value associated with key,
     *         or null if there was no mapping for key.
     */
    V insert(K key, V value);
    // Associates the specified value with the specified key in this map.
    // If the map previously contained a mapping for the key,
    // the old value is replaced by the specified value.
    // Returns the previous value associated with key,
    // or null if there was no mapping for key.

    /**
     * Returns the value for the given key.
     * @param key specified key.
     * @return the value to which the specified key is mapped,
     *         or null if this map contains no mapping for the key.
     */
    V search(K key);
    // Returns the value to which the specified key is mapped,
    // or null if this map contains no mapping for the key.

    /**
     * Removes the key-value-pair associated with the key.
     * @param key specified key.
     * @return the value to which the key was previously associated,
     *         or null if the key is not contained in the dictionary.
     */
    V remove(K key);
    // Removes the key-vaue-pair associated with the key.
    // Returns the value to which the key was previously associated,
    // or null if the key is not contained in the dictionary.

}
