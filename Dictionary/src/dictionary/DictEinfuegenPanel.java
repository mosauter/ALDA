// DictEinfuegenPanel.java

package dictionary;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;

/**
 * DictEinfuegenPanel is a Utility-Panel for the main DictGUI.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2014-10-07
 */
public class DictEinfuegenPanel extends JPanel {

    /**
     * Constant size of the TextFields.
     */
    private static final int COLUMNS = 20;
    /**
     * The Dictionary of the main GUI-file.
     */
    private final DictProxy<String, String> dict;

    /**
     * TextField for the german word.
     */
    private final JTextField germanText;
    /**
     * TextField for the english word.
     */
    private final JTextField englishText;
    /**
     * Button for the insertion.
     */
    private final JButton insert;
    /**
     * Button for the remove.
     */
    private final JButton remove;

    /**
     * public-constructor for the insert-Panel.
     * @param dict is the Dictionary of the main GUI-file.
     */
    public DictEinfuegenPanel(final DictProxy<String, String> dict) {
        this.dict = dict;

        JPanel labelPanel = new JPanel();
        labelPanel.setLayout(new GridLayout(2, 1));
        labelPanel.add(new JLabel("Deutsches Wort"));
        labelPanel.add(new JLabel("Englisches Wort"));

        germanText = new JTextField("", COLUMNS);
        englishText = new JTextField("", COLUMNS);

        JPanel textPanel = new JPanel(new GridLayout(2, 1));
        textPanel.add(germanText);
        textPanel.add(englishText);

        insert = new JButton("Einfuegen");
        insert.addActionListener(new InsertionListener());

        remove = new JButton("Loeschen");
        remove.addActionListener(new InsertionListener());

        TitledBorder border = BorderFactory.createTitledBorder("Einfuegen"
                + " / Loeschen");
        this.setBorder(border);
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(labelPanel);
        this.add(textPanel);
        this.add(insert);
        this.add(remove);
    }

    /**
     * This is the Listener to insert a new word-couple.
     */
    private class InsertionListener implements ActionListener {

        /**
         * public constructor.
         */
        public InsertionListener() {
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            if (e.getSource().equals(insert)) {
                String deutsch = germanText.getText();
                String english = englishText.getText();
                if (!deutsch.equals("") && !english.equals("")) {
                    dict.getDict().insert(deutsch, english);
                }
            } else if (e.getSource().equals(remove)) {
                String key = germanText.getText();
                if (!key.equals("")) {
                    dict.getDict().remove(key);
                }
            }
        }
    }
}
