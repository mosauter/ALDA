// TreeMapDictionary.java

package dictionary;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author mosauter
 */
public class HashMapDictionary<K, V> implements Dictionary<K, V> {

    private Map<K, V> map = new HashMap<>();
    
    @Override
    public V insert(K key, V value) {
        return map.put(key, value);
    }

    @Override
    public V search(K key) {
        return map.get(key);
    }

    @Override
    public V remove(K key) {
        return map.remove(key);
    }

    @Override
    public String toString() {
        Set<K> keys = map.keySet();
        StringBuilder sb = new StringBuilder();
        
        for (K x : keys) {
            sb.append(x).append(" ")
                    .append(map.get(x)).append("\n");
        }
        return sb.toString();
    }
}
