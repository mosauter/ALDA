// DictProxy.java

package dictionary;

/**
 * DictProxy is a Wrapper-Class for Dictionaries.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @param <K> first-generic-type.
 * @param <V> second-generic-type.
 * @since 2014-10-09
 */
public class DictProxy<K, V> {

    /**
     * Saves a Dictionary.
     */
    private Dictionary<K, V> dict;

    /**
     * Public-Constructor.
     * @param dict Dictionary.
     */
    public DictProxy(final Dictionary<K, V> dict) {
        this.dict = dict;
    }

    /**
     * Dictionary getter.
     * @return dict.
     */
    public final Dictionary<K, V> getDict() {
        return dict;
    }

    /**
     * Dictionary setter.
     * @param dict set as this.dict
     */
    public final void setDict(final Dictionary<K, V> dict) {
        this.dict = dict;
    }
}
