 // HashDictionary.java

package dictionary;

/**
 * HashDictionary is an own implementation of a HashMap-Dictionary.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @param <K> first generic-type
 * @param <V> second generic-type.
 * @since 2014-10-08
 */
public class HashDictionary<K, V> implements Dictionary<K, V> {

    /**
     * Private Entry-Object to save a key-value-pair with an optional
     * linked List.
     * @param <K> key type
     * @param <V> value type
     */
    private static class Entry<K, V> {
        /**
         * Optional Linked List.
         */
        private Entry<K, V> next;
        /**
         * Specified Key.
         */
        private final K key;
        /**
         * Specified Value.
         */
        private V value;

        /**
         * Public-Constructor.
         * @param e specified Entry
         * @param k new key
         * @param v new value
         */
        public Entry(final Entry e, final K k, final V v) {
            next = e;
            key = k;
            value = v;
        }
    }

    /**
     * Standard-Capacity.
     */
    private static final int PRIM_CAP = 16001;
    /**
     * Prime factor for the hash-method.
     */
    private static final int PRIM_STRING_INT = 31;

    /**
     * Entry-Array to save the key-value-pairs.
     */
    private final Entry<K, V>[] arr;

    /**
     * Constructor.
     */
    public HashDictionary() {
        arr = new Entry[PRIM_CAP];
    }

    @Override
    public final V insert(final K key, final V value) {
        int hash = hashCode(key.toString());

        if (arr[hash] == null) {
            arr[hash] = new Entry<>(null, key, value);
            return null;
        }
        Entry p = new Entry(arr[hash], null, null);
        while (p.next != null) {
            if (p.next.key == key) {
                V old = (V) p.next.value;
                p.next.value = value;
                return old;
            }
            p = p.next;
        }
        p.next = new Entry(null, key, value);
        return null;
    }

    @Override
    public final V search(final K key) {
        int hash = hashCode(key.toString());

        if (arr[hash] == null) {
            return null;
        }

        Entry p = new Entry(arr[hash], null, null);
        while (p.next != null) {
            if (p.next.key == key) {
                return ((V) p.next.value);
            }
            p = p.next;
        }
        return null;
    }

    @Override
    public final V remove(final K key) {
        int hash = hashCode(key.toString());

        if (arr[hash] == null) {
            return null;
        }

        Entry p = new Entry(arr[hash], null, null);
        Entry q = new Entry(arr[hash], null, null);
        while (p.next != null) {
            if (p.next.key == key) {
                V old = (V) p.next.value;
                p.next = p.next.next;
                arr[hash] = null;
                return old;
            }
            p = p.next;
            q = q.next;
        }
        return null;
    }

    /**
     * hash generates a integer out of an String.
     * @param key String
     * @return the hash to the specified string.
     */
    private int hash(String key) {
        int hash = 0;
        for (int i = 0; i < key.length(); i++) {
            hash = PRIM_STRING_INT * hash + key.charAt(i);
        }
        if (hash < 0) {
            return (-hash);
        }
        return hash;
    }

    /**
     * hashCode returns the hash code for a specified length of an array.
     * @param key specified string.
     * @return the hashCode to the specified string key.
     */
    private int hashCode(final String key) {
        int hash = hash(key);
        return (hash % PRIM_CAP);
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (Entry<K, V> x : arr) {
            if (x == null) {
                continue;
            }
            sb.append(x.key).append("  ").append(x.value).append("\n");
        }
        return sb.toString();
    }
}
