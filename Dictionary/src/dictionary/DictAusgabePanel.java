// DictAusgabePanel.java

package dictionary;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * DictAusgabePanel is a Utility-Panel for the main DictGUI.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 2.00
 * @since 2014-10-07
 */
public class DictAusgabePanel extends JPanel {

    /**
     * Saves if the test mode is enabled.
     */
    private boolean performance = false;
    /**
     * Constant size of the TextFields.
     */
    private static final int COLUMNS = 20;
    /**
     * dict is the english-german-Dictionary.
     */
    private final DictProxy<String, String> dict;

    /**
     * There is the german key-word.
     */
    private final JTextField searchDest;
    /**
     * search-Button.
     */
    private final JButton searchBut;

    /**
     * output-field for the english translation to the searchDest key.
     */
    private final JTextArea output;

    /**
     * public-constructor of a output and search Panel.
     * @param dict is the Dictionary of the main GUI file.
     */
    public DictAusgabePanel(final DictProxy<String, String> dict) {
        this.dict = dict;

        JPanel labelPanel = new JPanel(new GridLayout(1, 1));
        labelPanel.add(new JLabel("Deutsches Wort"));

        searchDest = new JTextField("", COLUMNS);

        JPanel textPanel = new JPanel(new GridLayout(1, 1));
        textPanel.add(searchDest);

        searchBut = new JButton("Suchen");
        searchBut.addActionListener(new SearchListener());

        JPanel searchPanel = new JPanel();
        searchPanel.setBorder(BorderFactory.createTitledBorder("Suchen"));
        searchPanel.setLayout(new GridLayout(1, 0));
        searchPanel.add(labelPanel);
        searchPanel.add(textPanel);
        searchPanel.add(searchBut);

        JPanel outputPanel = new JPanel(new BorderLayout());
        output = new JTextArea();
        JScrollPane js = new JScrollPane(output);
        js.setVisible(true);
        js.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);

        outputPanel.setBorder(BorderFactory.createTitledBorder("Ausgabe"));
        outputPanel.add(js);

        this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(searchPanel);
        this.add(outputPanel);
    }

    /**
     * SearchListener is for the search-Button.
     * It sets the output-Textfield on the value to the in the searchDest
     * specified key in the Dictionary dict.
     */
    private class SearchListener implements ActionListener {

        /**
         * public-constructor.
         */
        public SearchListener() {
        }

        @Override
        public void actionPerformed(final ActionEvent e) {
            if (performance) {
                performanceSearch();
                return;
            }

            String key = searchDest.getText();
            if ("".equals(key)) {
                output.setText(dict.getDict().toString());
                return;
            }
            String value = (String) dict.getDict().search(key);

            if (value == null) {
                output.setText("Leider keine Uebersetzung vorhanden!");
            } else {
                output.setText(value);
            }
        }
    }

    /**
     * Utility-Method for the performance-tests.
     */
    @SuppressWarnings("unchecked")
    private void performanceSearch() {
        Dictionary[] dictis = { new SortedArrayDictionary<>(),
                                new HashMapDictionary<>(),
                                new TreeMapDictionary<>(),
                                new HashDictionary<>(),
                                new TreeDictionary<>() };
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (Dictionary dicti : dictis) {
            Performance perf = new Performance(dicti);
            switch (i) {
                case 0:
                    sb.append("Sorted Array:\n");
                    break;
                case 1:
                    sb.append("Hash Map:\n");
                    break;
                case 2:
                    sb.append("Tree Map:\n");
                    break;
                case 3:
                    sb.append("Hash Dictionary:\n");
                    break;
                case 4:
                    sb.append("Tree Dictionary:\n");
                    break;
                default:
                    break;
            }
            sb.append(perf.performanceTest());
            i++;
        }
        this.output.setText(sb.toString());
    }

    /**
     * Performance setter.
     * @param performance set as this.performance.
     */
    public final void setPerformance(final boolean performance) {
        this.performance = performance;
    }
}
