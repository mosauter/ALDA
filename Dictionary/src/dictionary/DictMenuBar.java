// DictMenuBar.java

package dictionary;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;

/**
 * DictMenuBar is the JMenuBar for the DictGUI.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.21
 * @since 2014-10-07
 */
public class DictMenuBar extends JMenuBar {

    /**
     * The dictionary of the main GUI-file.
     */
    private final DictProxy<String, String> dict;

    /**
     * JMenu file - "Datei".
     */
    private final JMenu file;
    /**
     * JMenuItem read - "Woerterbuch einlesen".
     */
    private final JMenuItem read;
    /**
     * JMenuItem close - "Schliessen".
     */
    private final JMenuItem close;

    /**
     * JMenu performMenu - "Performance Test".
     */
    private final JMenu performMenu;
    /**
     * JCheckBox performance - "Performance".
     */
    private final JCheckBox performance;
    /**
     * outputPanel-reference.
     */
    private final DictAusgabePanel outputPanel;

    /**
     * JMenu implementation as the menu for the choice of the implementation.
     */
    private final JMenu implementation;
    /**
     * JRadioButton for the Sorted-Array-Implementation.
     */
    private final JRadioButton sortedArray;
    /**
     * JRadioButton for the Tree-Map-Java-Collection-Implementation.
     */
    private final JRadioButton treeMap;
    /**
     * JRadioButton for the Hash-Map-Java-Collection-Implementation.
     */
    private final JRadioButton hashMap;
    /**
     * JRadioButton for the Hash-Dictionary-Implementation.
     */
    private final JRadioButton hashDict;
    /**
     * JRadioButton for the Tree-Dictionary-Implementation.
     */
    private final JRadioButton treeDict;

    /**
     * Public-Constructor.
     * @param dict the dictionary.
     * @param outputPanel reference for the outputPanel.
     */
    public DictMenuBar(final DictProxy<String, String> dict,
            final DictAusgabePanel outputPanel) {
        this.dict = dict;
        this.outputPanel = outputPanel;

        file = new JMenu("Datei");
        read = new JMenuItem("Woerterbuch laden");
        close = new JMenuItem("Schliessen");

        performMenu = new JMenu("Performance Test");
        performance = new JCheckBox("Performance Test");
        performance.addItemListener(new PerfListener());
        performMenu.add(performance);

        implementation = new JMenu("Implementierung");
        sortedArray = new JRadioButton("Sorted Array Dictionary");
        treeMap = new JRadioButton("Tree Map Dictionary");
        hashMap = new JRadioButton("Hash Map Dictionary");
        hashDict = new JRadioButton("Hash Dictionary");
        treeDict = new JRadioButton("Tree Dictionary");

        ButtonGroup impl = new ButtonGroup();
        impl.add(sortedArray);
        impl.add(treeMap);
        impl.add(hashMap);
        impl.add(hashDict);
        impl.add(treeDict);
        sortedArray.setSelected(true);

        read.addActionListener(new ReadListener());
        close.addActionListener(new CloseListener());
        sortedArray.addActionListener(new ImplementationListener());
        treeMap.addActionListener(new ImplementationListener());
        hashMap.addActionListener(new ImplementationListener());
        hashDict.addActionListener(new ImplementationListener());
        treeDict.addActionListener(new ImplementationListener());

        implementation.add(sortedArray);
        implementation.add(treeMap);
        implementation.add(hashMap);
        implementation.add(hashDict);
        implementation.add(treeDict);

        file.add(implementation);
        file.add(read);
        file.add(close);

        this.add(file);
        this.add(performMenu);
    }

    /**
     * Listener for the CheckBox performance.
     * To activate the Performance-Test-Mode.
     */
    private class PerfListener implements ItemListener {

        @Override
        public void itemStateChanged(final ItemEvent e) {
            if (e.getStateChange() == ItemEvent.SELECTED) {
                outputPanel.setPerformance(true);
            } else {
                outputPanel.setPerformance(false);
            }
        }
    }

    /**
     * CloseListener closes the application after confirmation.
     */
    private class CloseListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            int result = JOptionPane.showConfirmDialog(
                        null,
                        "Are you sure you want to exit the application?",
                        "Exit Application",
                        JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    System.exit(0);
                }
        }
    }

    /**
     * ReadListener is reads a Dict-File.txt and insert it in the dict-variable.
     */
    private class ReadListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            JFileChooser fc = new JFileChooser();
            int returnVal = fc.showOpenDialog(getParent());

            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File file = fc.getSelectedFile();
                read(file);
            }
        }
    }

    /**
     * Utility-Method for the ReadListener.
     * @param f is the name of the Target Dict-File.txt.
     */
    private void read(final File f) {
        LineNumberReader in;
        try {
            in = new LineNumberReader(new FileReader(f));
            String line;
            while ((line = in.readLine()) != null) {
                String[] words = line.split(" ");
                if (words.length == 2) {
                    dict.getDict().insert(words[0], words[1]);
                }
            }
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(Dictionary.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    /**
     * ImplementationListener is for the choice of the implementation.
     */
    private class ImplementationListener implements ActionListener {

        @Override
        public void actionPerformed(final ActionEvent e) {
            JRadioButton source = (JRadioButton) e.getSource();
            switch (source.getText()) {
            case "Sorted Array Dictionary":
                dict.setDict(new SortedArrayDictionary<String, String>());
                break;
            case "Tree Map Dictionary":
                dict.setDict(new TreeMapDictionary<String, String>());
                break;
            case "Hash Map Dictionary":
                dict.setDict(new HashMapDictionary<String, String>());
                break;
            case "Hash Dictionary":
                dict.setDict(new HashDictionary<String, String>());
                break;
            case "Tree Dictionary":
                dict.setDict(new TreeDictionary<String, String>());
                break;
            default:
                break;
            }
        }
    }
}
