// TreeMapDictionary.java

package dictionary;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

/**
 * TreeMapDictionary Implementation for Dictionary with Java-Collection.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @param <K> first generic-type.
 * @param <V> second generic-type.
 * @version 1.00
 * @since 2014-10-08
 */
public class TreeMapDictionary<K, V> implements Dictionary<K, V> {

    /**
     * Map to save key and value.
     */
    private final Map<K, V> map = new TreeMap<>();

    @Override
    public final V insert(final K key, final V value) {
        return map.put(key, value);
    }

    @Override
    public final V search(final K key) {
        return map.get(key);
    }

    @Override
    public final V remove(final K key) {
        return map.remove(key);
    }

    @Override
    public final String toString() {
        Set<K> keys = map.keySet();
        StringBuilder sb = new StringBuilder();

        for (K x : keys) {
            sb.append(x).append(" ").append(map.get(x));
        }
        return sb.toString();
    }
}
