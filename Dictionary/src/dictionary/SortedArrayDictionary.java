// SortedArrayDictionary.java

package dictionary;

/**
 * SortedArrayDictionary is Task 1 in Semester 3.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 4.00
 * @param <K> first generic-type.
 * @param <V> second generic-type.
 * @since 2014-10-06
 */
public class SortedArrayDictionary<K extends Comparable<? super K>, V>
implements Dictionary<K, V> {

    /**
     * Entry-Class to save a key-value-Pair in one object.
     * @param <K>
     * @param <V>
     */
    private static class Entry<K, V> {
        /**
         * Key.
         */
        private K key;
        /**
         * Value.
         */
        private V value;

        /**
         * Public-Constructor.
         * @param k the new key.
         * @param v the new value.
         */
        public Entry(final K k, final V v) { key = k; value = v; }
    }

    /**
     * DEF_CAP is the start-length of the Arrays.
     */
    private static final int DEF_CAP = 16;

    /**
     * EntryArray for the Dictionary.
     */
    private Entry<K, V>[] arr;

    /**
     * size saves the size of the Dictionary.
     */
    private int size;

    /**
     * Constructor.
     */
    @SuppressWarnings("unchecked")
    public SortedArrayDictionary() {
        size = 0;
        arr = new Entry[DEF_CAP];
    }

    @Override
    public final V insert(final K key, final V value) {
        int pos = searchKey(key);

        // Ueberschreiben von altem Eintrag
        if (pos != -1) {
            V old = arr[pos].value;
            arr[pos].value = value;
            return old;
        }

        // Falls notwendig, das Feld vergroessern
        if (size == this.arr.length) {
            ensureCapacity(2 * size);
        }

        // Neuer Eintrag
        int i = size - 1;
        while (i >= 0 && key.compareTo(arr[i].key) < 0) {
            arr[i + 1] = arr[i];
            i--;
        }
        this.arr[i + 1] = new Entry<>(key, value);
        size++;
        return null;
    }

    @Override
    public final V search(final K key) {
        int position = searchKey(key);

        if (position < 0) {
            return null;
        } else {
            return arr[position].value;
        }
    }


    @Override
    public final V remove(final K key) {
        int i;
        V cop = null;
        for (i = 0; i < size; i++) {
            if (this.arr[i].key.equals(key)) {
                cop = this.arr[i].value;
                break;
            }
        }
        if (i == size) {
            return null;
        }
        size--;
        for (int j = i; j < size; j++) {
            this.arr[j].key = this.arr[j + 1].key;
            this.arr[j].value = this.arr[j + 1].value;
        }
        return cop;
    }

    @Override
    public final String toString() {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            sb.append("").append(this.arr[i].key).append("  ")
                    .append(this.arr[i].value).append("\n");
        }
        return sb.toString();
    }

    /**
     * Utility-Method to ensure the capacity of the arr-array.
     * @param newCap is the new capacity.
     */
    @SuppressWarnings("unchecked")
    private void ensureCapacity(final int newCap) {
        if (newCap < size) {
            return;
        }
        Entry[] old = arr;
        arr = new Entry[newCap];
        System.arraycopy(old, 0, arr, 0, size);
    }

    /**
     * searchKey is a binary Search.
     * It searchs the position of the key-value-pair to the specified key.
     * @param key specified key
     * @return the position of the key-value-pair or -1 if there isn't a value
     *         to the specified key.
     */
    public final int searchKey(final K key) {
        int left = 0;
        int right = size - 1;

        while (right >= left) {
            int m = (right + left) / 2;

            if (arr[m].key.compareTo(key) > 0) {
                right = m - 1;
            } else if (arr[m].key.compareTo(key) < 0) {
                left = m + 1;
            } else {
                return m;
            }
        }
        return -1;
    }
}