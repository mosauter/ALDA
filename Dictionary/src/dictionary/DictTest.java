// DictTest.java

package dictionary;

/**
 * DictTest.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2014-10-06
 */
public final class DictTest {

    /**
     * private constructor.
     */
    private DictTest() { }

    /**
     * main DictTest.
     * @param args not used
     */
    public static void main(final String[] args) {
        Dictionary<String, String> dict = new TreeDictionary<>();
        dict.insert("Hallo", "1");
        dict.insert("wie", "2");
        dict.insert("geht", "3");
        dict.insert("es", "4");
        dict.insert("dir", "5");
        dict.insert("?", "6");
        dict.insert("dT", "14");
        System.out.println(dict.search("Hallo"));   // 1
        System.out.println(dict.remove("dirg"));    // null
        System.out.println(dict.insert("?", "10")); // 6
        System.out.println(dict.remove("dir"));     // 5
        System.out.println(dict.remove("dir"));     // null
        System.out.println(dict);
        new DictGUI();
    }
}
