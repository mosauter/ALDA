// DictGUI.java

package dictionary;

import java.awt.Dimension;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 * GUI for Dictionaries.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2014-10-07
 */
public class DictGUI extends JFrame {
    /**
     * BORDER is the constant distance for the border.
     */
    private static final int BORDER = 10;
    /**
     * The Dictionary which is referenced to all the other GUI-components.
     */
    private final DictProxy<String, String> dict;

    /**
     * Public-Constructor.
     */
    public DictGUI() {
        this.dict = new DictProxy<>(
                new SortedArrayDictionary<String, String>());
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(final WindowEvent e) {
                JFrame frame = (JFrame) e.getSource();

                int result = JOptionPane.showConfirmDialog(
                        frame,
                        "Are you sure you want to exit the application?",
                        "Exit Application",
                        JOptionPane.YES_NO_OPTION);

                if (result == JOptionPane.YES_OPTION) {
                    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                }
            }
        });
        this.setTitle("Dictionary");

        JPanel mainPanel = new JPanel();
        mainPanel.setBorder(BorderFactory.createEmptyBorder(BORDER,
                BORDER, BORDER, BORDER));
        mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.PAGE_AXIS));

        DictAusgabePanel outputPanel = new DictAusgabePanel(dict);
        mainPanel.add(new DictEinfuegenPanel(dict));
        mainPanel.add(outputPanel);
        this.setJMenuBar(new DictMenuBar(dict, outputPanel));

        this.setMinimumSize(new Dimension(485, 501));
        this.setContentPane(mainPanel);
        pack();
        setVisible(true);
        toFront();
    }
}
