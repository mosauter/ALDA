// Performance.java

package dictionary;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Performance is an Utility-Class for Performance-Testing.
 * @author Moritz Sauter (SauterMoritz@gmx.de)
 * @version 1.00
 * @since 2014-10-13
 */
public class Performance {
    /**
     * Dictionary in an implementation.
     */
    private final Dictionary<String, String> dict;
    /**
     * Factor for the milliseconds.
     */
    private static final double MILLISEK = 1000000.;

    /**
     * Public-Constructor.
     * @param dict the Dictionary in an Implementation.
     */
    public Performance(final Dictionary<String, String> dict) {
        this.dict = dict;
    }

    /**
     * perfCreate is for performance testing the creation of a Dictionary.
     * @param path where the key-value-pairs come from.
     * @return the time of the creation.
     */
    private double perfCreate(final String path) {
        long startTime = System.nanoTime();
        File file = new File(path);
        LineNumberReader in;
        try {
            in = new LineNumberReader(new FileReader(file));
            String line;
            while ((line = in.readLine()) != null) {
                String[] words = line.split(" ");
                dict.insert(words[0], words[1]);
            }
            in.close();
        } catch (IOException ex) {
            Logger.getLogger(Dictionary.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
        long endTime = (System.nanoTime() - startTime);
        double time = endTime / MILLISEK;
        return time;
    }

    /**
     * perfSearch is for performance testing an success-search
     * or a non-success-search.
     * @param path to the txt-file from where the search keys come.
     * @param art if the search should be success or not.
     * @return the time of the search.
     */
    private double perfSearch(final String path, final boolean art) {
        File file = new File(path);
        try {
            LineNumberReader in = new LineNumberReader(new FileReader(file));
            String line;
            while ((line = in.readLine()) != null) {
                String[] words = line.split(" ");
                dict.insert(words[0], words[1]);
            }
            long start = System.nanoTime();
            in = new LineNumberReader(new FileReader(file));
            while ((line = in.readLine()) != null) {
                String[] words = line.split(" ");
                if (art) {
                    dict.search(words[0]);
                } else {
                    dict.search(words[1]);
                }
            }
            in.close();
            double time = (System.nanoTime() - start) / MILLISEK;
            return time;
        } catch (IOException ex) {
            Logger.getLogger(Dictionary.class.getName())
                    .log(Level.SEVERE, null, ex);
            return -1;
        }
    }

    /**
     * Starts a performance Test with the Dictionary dict.
     * @return a formatted String for e.g. an output.
     */
    public final String performanceTest() {
        String[] paths = {"/home/moritz/Dokumente/NetBeansProjects/"
                + "Dictionary/src/dictionary/dtengl8000.txt",
            "/home/moritz/Dokumente/NetBeansProjects/"
                + "Dictionary/src/dictionary/dtengl16000.txt" };
        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (String path : paths) {
            switch (i) {
                case 0:
                    sb.append("n = 8000\n");
                    i++;
                    break;
                case 1:
                    sb.append("n = 16000\n");
                    i++;
                    break;
                default:
                    break;
            }
            sb.append("Creation:\t\t").append(perfCreate(path)).append(" ms\n");
            sb.append("Success-Search:\t").append(perfSearch(path, true))
                    .append(" ms\n");
            sb.append("Non-success-Search:\t").append(perfSearch(path, false))
                    .append(" ms\n\n");
        }
        return sb.toString();
    }
}
